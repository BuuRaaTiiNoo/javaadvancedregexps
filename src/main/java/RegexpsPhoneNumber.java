import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpsPhoneNumber {

    private static final String filePath = "C:\\Users\\Vladimir_Kadrov\\IdeaProjects\\regexps\\src\\main\\resources\\text.txt";
    private static final String formattedFilePath = "C:\\Users\\Vladimir_Kadrov\\IdeaProjects\\regexps\\src\\main\\resources\\formattedText.txt";

    private String numberFormatter(String number) {
        return number.replaceAll("[\\D+]", "");
    }

    private String numberReplace(String text) {
        String result = "";
        Pattern regexp = Pattern.compile("\\+([\\d])\\(\\d{3}\\)[ ]\\d{3}([ ]\\d{2}){2}");
        Matcher matcher = regexp.matcher(text);
        while (matcher.find()) {
            result = result.concat(numberFormatter(matcher.group()) + "\n");
        }
        return result;
    }


    private void stringToFile(String phoneNumbers) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(formattedFilePath))) {
            bufferedWriter.write(phoneNumbers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String fileToString() {
        String line = null;
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            line = bufferedReader.readLine();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }

    public static void main(String[] args) {
        RegexpsPhoneNumber regexpsPhoneNumber = new RegexpsPhoneNumber();
        String text = regexpsPhoneNumber.fileToString();
        String formatText = regexpsPhoneNumber.numberReplace(text);
        regexpsPhoneNumber.stringToFile(formatText);
    }
}
